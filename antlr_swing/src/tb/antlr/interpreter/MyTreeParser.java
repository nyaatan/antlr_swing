package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {

	private LocalSymbols vars = new LocalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
    public void newVar(String s) {
        vars.newSymbol(s);
    }
    
    public Integer setVar(String s, Integer e) {
        vars.setSymbol(s, e);
        return e;
    }
    
    public Integer getVar(String s) {
        return vars.getSymbol(s);
    }

}
